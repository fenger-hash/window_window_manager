# Copyright (c) 2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/test.gni")
import("../../../windowmanager_aafwk.gni")
module_out_path = "window_manager/window_scene"

group("unittest") {
  testonly = true

  deps = [
    ":ws_anomaly_detection_test",
    ":ws_distributed_client_test",
    ":ws_dual_display_device_policy_test",
    ":ws_event_stage_test",
    ":ws_extension_session_manager_test",
    ":ws_extension_session_test",
    ":ws_fold_screen_controller_test",
    ":ws_fold_screen_state_machine_test",
    ":ws_intention_event_manager_test",
    ":ws_keyboard_session_test",
    ":ws_main_session_test",
    ":ws_move_drag_controller_test",
    ":ws_root_scene_session_test",
    ":ws_scb_system_session_test",
    ":ws_scene_input_manager_test",
    ":ws_scene_persistence_test",
    ":ws_scene_persistent_storage_test",
    ":ws_scene_session_converter_test",
    ":ws_scene_session_dirty_manager_test",
    ":ws_scene_session_manager_lite_stub_test",
    ":ws_scene_session_manager_proxy_test",
    ":ws_scene_session_manager_stub_test",
    ":ws_scene_session_manager_test",
    ":ws_scene_session_test",
    ":ws_screen_cutout_controller_test",
    ":ws_screen_property_test",
    ":ws_screen_rotation_property_test",
    ":ws_screen_scene_config_test",
    ":ws_screen_session_manager_client_proxy_test",
    ":ws_screen_session_manager_client_stub_test",
    ":ws_screen_session_manager_client_test",
    ":ws_screen_session_manager_proxy_test",
    ":ws_screen_session_manager_stub_test",
    ":ws_screen_session_manager_test",
    ":ws_screen_session_test",
    ":ws_screen_snapshot_ability_connection_test",
    ":ws_screen_snapshot_picker_test",
    ":ws_session_display_power_controller_test",
    ":ws_session_listener_controller_test",
    ":ws_session_manager_agent_controller_test",
    ":ws_session_manager_service_recover_proxy_test",
    ":ws_session_manager_test",
    ":ws_session_permission_test",
    ":ws_session_proxy_mock_test",
    ":ws_session_proxy_test",
    ":ws_session_stage_proxy_test",
    ":ws_session_stage_stub_test",
    ":ws_session_stub_mock_test",
    ":ws_session_stub_test",
    ":ws_session_test",
    ":ws_setting_observer_test",
    ":ws_sub_session_test",
    ":ws_system_session_test",
    ":ws_task_scheduler_test",
    ":ws_timer_manager_test",
    ":ws_window_event_channel_proxy_mock_test",
    ":ws_window_event_channel_proxy_test",
    ":ws_window_event_channel_stub_mock_test",
    ":ws_window_event_channel_stub_test",
    ":ws_window_event_channel_test",
    ":ws_window_scene_config_test",
    ":ws_window_session_property_test",
  ]
}

ohos_unittest("ws_anomaly_detection_test") {
  module_out_path = module_out_path

  sources = [ "anomaly_detection_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "ability_base:session_info",
    "ability_runtime:ability_context_native",
    "ability_runtime:mission_info",
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_scene_input_manager_test") {
  module_out_path = module_out_path

  sources = [ "scene_input_manager_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "ability_runtime:app_manager",
    "accessibility:accessibility_common",
    "ace_engine:ace_uicontent",
    "c_utils:utils",
    "eventhandler:libeventhandler",
    "graphic_2d:libcomposer",
    "graphic_2d:window_animation",
    "hilog:libhilog",
    "hisysevent:libhisysevent",
    "hitrace:hitrace_meter",
    "input:libmmi-client",
  ]
}

ohos_unittest("ws_system_session_test") {
  module_out_path = module_out_path

  sources = [ "system_session_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_keyboard_session_test") {
  module_out_path = module_out_path

  sources = [ "keyboard_session_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
    "init:libbegetutil",
  ]
}

ohos_unittest("ws_dual_display_device_policy_test") {
  module_out_path = module_out_path

  sources = [ "dual_display_device_policy_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "ability_runtime:app_manager",
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_main_session_test") {
  module_out_path = module_out_path

  sources = [ "main_session_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_scb_system_session_test") {
  module_out_path = module_out_path

  sources = [ "scb_system_session_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_sub_session_test") {
  module_out_path = module_out_path

  sources = [ "sub_session_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_task_scheduler_test") {
  module_out_path = module_out_path

  sources = [ "task_scheduler_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_session_permission_test") {
  module_out_path = module_out_path

  sources = [ "session_permission_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_session_listener_controller_test") {
  module_out_path = module_out_path

  sources = [ "session_listener_controller_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "ability_runtime:ability_manager",
    "c_utils:utils",
    "image_framework:image_native",
  ]
}

ohos_unittest("ws_event_stage_test") {
  module_out_path = module_out_path

  sources = [ "event_stage_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "ability_base:session_info",
    "ability_base:want",
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_scene_persistent_storage_test") {
  module_out_path = module_out_path

  sources = [ "scene_persistent_storage_test.cpp" ]

  include_dirs = [ "${window_base_path}/window_scene/session/host/include" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "ability_base:session_info",
    "ability_base:want",
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_screen_session_test") {
  module_out_path = module_out_path

  include_dirs = [ "${window_base_path}/window_scene/session/screen/include" ]
  sources = [ "screen_session_test.cpp" ]

  deps = [
    ":ws_unittest_common",
    "${window_base_path}/window_scene/session:screen_session",
  ]

  external_deps = [ "c_utils:utils" ]
}

ohos_unittest("ws_screen_snapshot_ability_connection_test") {
  module_out_path = module_out_path

  include_dirs = [ "${window_base_path}/window_scene/session/screen/include" ]
  sources = [ "screen_snapshot_ability_connection_test.cpp" ]

  deps = [
    ":ws_unittest_common",
    "${window_base_path}/window_scene/session:screen_session",
  ]

  external_deps = [
    "ability_base:base",
    "ability_base:want",
    "ability_runtime:abilitykit_native",
    "ability_runtime:extension_manager",
    "c_utils:utils",
    "ipc:ipc_core",
  ]
}

ohos_unittest("ws_screen_snapshot_picker_test") {
  module_out_path = module_out_path

  include_dirs = [ "${window_base_path}/window_scene/session/screen/include" ]
  sources = [ "screen_snapshot_picker_test.cpp" ]

  deps = [
    ":ws_unittest_common",
    "${window_base_path}/window_scene/session:screen_session",
  ]

  external_deps = [
    "ability_runtime:abilitykit_native",
    "c_utils:utils",
  ]
}

ohos_unittest("ws_extension_session_test") {
  module_out_path = module_out_path

  sources = [ "extension_session_test.cpp" ]
  include_dirs = [ "${window_base_path}/window_scene/session/host/include" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "ability_base:session_info",
    "ability_base:want",
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_root_scene_session_test") {
  module_out_path = module_out_path

  sources = [ "root_scene_session_test.cpp" ]

  include_dirs = [ "${window_base_path}/window_scene/session/host/include" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_scene_persistence_test") {
  module_out_path = module_out_path

  sources = [ "scene_persistence_test.cpp" ]

  include_dirs = [ "${window_base_path}/window_scene/session/host/include" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_screen_property_test") {
  module_out_path = module_out_path

  include_dirs = [ "${window_base_path}/window_scene/session/screen/include" ]
  sources = [ "screen_property_test.cpp" ]

  deps = [
    ":ws_unittest_common",
    "${window_base_path}/window_scene/session:screen_session",
  ]

  external_deps = [
    "c_utils:utils",
    "graphic_2d:librender_service_client",
  ]
}

ohos_unittest("ws_session_test") {
  module_out_path = module_out_path

  sources = [ "session_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "ability_base:session_info",
    "ability_base:want",
    "ability_runtime:app_manager",
    "accessibility:accessibility_common",
    "c_utils:utils",
    "eventhandler:libeventhandler",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_timer_manager_test") {
  module_out_path = module_out_path

  sources = [ "timer_manager_test.cpp" ]

  deps = [
    ":ws_unittest_common",
    "${window_base_path}/window_scene/intention_event/service:intention_event_anr_manager",
  ]

  external_deps = [
    "ability_base:session_info",
    "ability_base:want",
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_screen_cutout_controller_test") {
  module_out_path = module_out_path

  sources = [ "screen_cutout_controller_test.cpp" ]

  deps = [ ":ws_unittest_common" ]
  external_deps = [
    "ability_runtime:app_manager",
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_screen_scene_config_test") {
  module_out_path = module_out_path

  sources = [ "screen_scene_config_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_screen_session_manager_client_proxy_test") {
  module_out_path = module_out_path

  sources = [ "screen_session_manager_client_proxy_test.cpp" ]

  deps = [ ":ws_unittest_common" ]
}

ohos_unittest("ws_screen_session_manager_client_stub_test") {
  module_out_path = module_out_path

  sources = [ "screen_session_manager_client_stub_test.cpp" ]

  deps = [ ":ws_unittest_common" ]
}

ohos_unittest("ws_screen_session_manager_client_test") {
  module_out_path = module_out_path

  sources = [ "screen_session_manager_client_test.cpp" ]

  deps = [ ":ws_unittest_common" ]
}

ohos_unittest("ws_screen_session_manager_stub_test") {
  module_out_path = module_out_path

  sources = [ "screen_session_manager_stub_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "ability_runtime:app_manager",
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_screen_session_manager_proxy_test") {
  module_out_path = module_out_path

  sources = [ "screen_session_manager_proxy_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "ability_runtime:app_manager",
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_screen_session_manager_test") {
  module_out_path = module_out_path

  sources = [ "screen_session_manager_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "ability_runtime:app_manager",
    "ability_runtime:runtime",
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_fold_screen_controller_test") {
  module_out_path = module_out_path

  sources = [ "fold_screen_controller_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "ability_runtime:app_manager",
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_window_scene_config_test") {
  module_out_path = module_out_path

  sources = [ "window_scene_config_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_scene_session_manager_test") {
  module_out_path = module_out_path

  sources = [ "scene_session_manager_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "ability_base:configuration",
    "ability_base:session_info",
    "ability_runtime:ability_context_native",
    "ability_runtime:mission_info",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "eventhandler:libeventhandler",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_scene_session_manager_proxy_test") {
  module_out_path = module_out_path

  sources = [ "scene_session_manager_proxy_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "ability_runtime:app_manager",
    "ability_runtime:mission_info",
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_session_manager_agent_controller_test") {
  module_out_path = module_out_path

  sources = [ "session_manager_agent_controller_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "ability_runtime:app_manager",
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_session_manager_test") {
  module_out_path = module_out_path

  sources = [ "session_manager_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_scene_session_manager_stub_test") {
  module_out_path = module_out_path

  sources = [ "scene_session_manager_stub_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "ability_runtime:app_manager",
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_scene_session_test") {
  module_out_path = module_out_path

  sources = [ "scene_session_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "ability_base:session_info",
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_scene_session_dirty_manager_test") {
  module_out_path = module_out_path

  sources = [ "scene_session_dirty_manager_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "ability_base:configuration",
    "ability_base:session_info",
    "ability_runtime:ability_context_native",
    "ability_runtime:mission_info",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "hilog:libhilog",
    "input:libmmi-client",
  ]
}

ohos_unittest("ws_window_session_property_test") {
  module_out_path = module_out_path

  sources = [ "window_session_property_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [ "c_utils:utils" ]
}

ohos_unittest("ws_window_event_channel_test") {
  module_out_path = module_out_path

  sources = [ "window_event_channel_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_screen_rotation_property_test") {
  module_out_path = module_out_path

  sources = [ "screen_rotation_property_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "ability_runtime:app_manager",
    "c_utils:utils",
    "hilog:libhilog",
  ]
  defines = []
  if (defined(global_parts_info) && defined(global_parts_info.sensors_sensor)) {
    external_deps += [ "sensor:sensor_interface_native" ]
    defines += [ "SENSOR_ENABLE" ]
  }
}

ohos_unittest("ws_screen_session_dumper_test") {
  module_out_path = module_out_path

  sources = [ "ws_screen_session_dumper_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_move_drag_controller_test") {
  module_out_path = module_out_path

  sources = [ "move_drag_controller_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_session_proxy_test") {
  module_out_path = module_out_path

  include_dirs =
      [ "${window_base_path}/window_scene/session/host/include/zidl" ]
  sources = [ "session_proxy_test.cpp" ]

  deps = [
    ":ws_unittest_common",
    "${window_base_path}/window_scene/session:screen_session",
  ]

  external_deps = [
    "ability_base:session_info",
    "ability_base:want",
    "c_utils:utils",
  ]
}

ohos_unittest("ws_session_proxy_mock_test") {
  module_out_path = module_out_path

  include_dirs = [
    "${window_base_path}/window_scene/session/host/include/zidl",
    "${window_base_path}/window_scene/test/mock",
  ]

  sources = [
    "../mock/mock_message_parcel.cpp",
    "session_proxy_mock_test.cpp",
  ]

  deps = [
    ":ws_unittest_common",
    "${window_base_path}/window_scene/session:screen_session",
  ]

  external_deps = [
    "ability_base:session_info",
    "ability_base:want",
    "c_utils:utils",
  ]
}
ohos_unittest("ws_session_display_power_controller_test") {
  module_out_path = module_out_path

  sources = [ "session_display_power_controller_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "ability_runtime:app_manager",
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_scene_session_converter_test") {
  module_out_path = module_out_path

  sources = [ "scene_session_converter_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "ability_runtime:app_manager",
    "ability_runtime:mission_info",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_session_stage_proxy_test") {
  module_out_path = module_out_path

  sources = [ "session_stage_proxy_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "ability_base:base",
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_session_stage_stub_test") {
  module_out_path = module_out_path

  sources = [ "session_stage_stub_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "ability_runtime:app_manager",
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_window_event_channel_proxy_test") {
  module_out_path = module_out_path

  sources = [ "window_event_channel_proxy_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_window_event_channel_proxy_mock_test") {
  module_out_path = module_out_path

  sources = [
    "../mock/mock_message_parcel.cpp",
    "window_event_channel_proxy_mock_test.cpp",
  ]

  include_dirs = [ "${window_base_path}/window_scene/test/mock" ]

  defines = [
    "ENABLE_MOCK_WRITE_STRING",
    "ENABLE_MOCK_WRITE_STRING_VECTOR",
    "ENABLE_MOCK_READ_INT64",
  ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_extension_session_manager_test") {
  module_out_path = module_out_path

  sources = [ "extension_session_manager_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_window_event_channel_stub_test") {
  module_out_path = module_out_path

  sources = [ "window_event_channel_stub_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "ability_runtime:app_manager",
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_window_event_channel_stub_mock_test") {
  module_out_path = module_out_path

  sources = [
    "../mock/mock_message_parcel.cpp",
    "window_event_channel_stub_mock_test.cpp",
  ]

  include_dirs = [ "${window_base_path}/window_scene/test/mock" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "ability_runtime:app_manager",
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_fold_screen_state_machine_test") {
  module_out_path = module_out_path

  sources = [ "fold_screen_state_machine_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "ability_runtime:app_manager",
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_session_stub_test") {
  module_out_path = module_out_path

  sources = [ "session_stub_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
  ]

  public_external_deps = [ "accessibility:accessibility_interface" ]
}

ohos_unittest("ws_session_stub_mock_test") {
  module_out_path = module_out_path

  sources = [
    "../mock/mock_message_parcel.cpp",
    "session_stub_mock_test.cpp",
  ]

  defines = [ "ENABLE_MOCK_READ_INT64" ]

  include_dirs = [ "${window_base_path}/window_scene/test/mock" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
  ]

  public_external_deps = [ "accessibility:accessibility_interface" ]
}

ohos_unittest("ws_distributed_client_test") {
  module_out_path = module_out_path

  sources = [ "distributed_client_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_setting_observer_test") {
  module_out_path = module_out_path

  sources = [ "setting_observer_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "ability_runtime:ability_manager",
    "ability_runtime:dataobs_manager",
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_intention_event_manager_test") {
  module_out_path = module_out_path

  sources = [ "intention_event_manager_test.cpp" ]

  deps = [
    ":ws_unittest_common",
    "${window_base_path}/window_scene/intention_event:libintention_event",
  ]

  external_deps = [
    "ability_runtime:mission_info",
    "ace_engine:ace_uicontent",
    "c_utils:utils",
    "eventhandler:libeventhandler",
    "hilog:libhilog",
    "input:libmmi-client",
  ]
}

ohos_unittest("ws_scene_session_manager_lite_stub_test") {
  module_out_path = module_out_path

  sources = [ "scene_session_manager_lite_stub_test.cpp" ]

  deps = [ ":ws_unittest_common" ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("ws_session_manager_service_recover_proxy_test") {
  module_out_path = module_out_path

  include_dirs = [ "../mock" ]

  sources = [
    "../mock/mock_message_parcel.cpp",
    "session_manager_service_recover_proxy_test.cpp",
  ]

  deps = [
    ":ws_unittest_common",
    "${window_base_path}/wmserver:sms",
  ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

## Build ws_unittest_common.a {{{
config("ws_unittest_common_public_config") {
  include_dirs = [
    "${window_base_path}/test/common/mock",

    # for WMError Code
    "${window_base_path}/dm/include",
    "${window_base_path}/dmserver/include",
    "${window_base_path}/interfaces/innerkits/dm",
    "${window_base_path}/interfaces/innerkits/wm",
    "${window_base_path}/window_scene/screen_session_manager/include",
    "${window_base_path}/window_scene/session_manager/include",
    "${window_base_path}/window_scene/session_manager_service/include",
    "${window_base_path}/window_scene/common/include",
    "${window_base_path}/window_scene",
    "${window_base_path}/window_scene/test",
    "${window_base_path}/window_scene/intention_event/service/event_stage/include",
    "//third_party/googletest/googlemock/include",

    # for window_manager_interface
    "${window_base_path}/wmserver/include",
    "${window_base_path}/wmserver/include/zidl",
    "${graphic_base_path}/graphic_2d/rosen/modules/animation/window_animation/include",
    "${multimodalinput_path}/input/interfaces/native/innerkits/event/include",
    "${multimodalinput_path}/input/util/common/include",
    "${ability_runtime_inner_api_path}/ability_manager/include",
    "${window_base_path}/wm/include",
    "${window_base_path}/wm/include/zidl",

    # for session
    "${window_base_path}/window_scene/session/container/include/zidl",

    # for session_manager
    "${multimodalinput_path}/input/interfaces/native/innerkits/event/include",
    "${window_base_path}/window_scene/interfaces/include",
    "${resourceschedule_path}/ffrt/interfaces/kits",
    "${ability_runtime_inner_api_path}/ability_manager/include",
    "${ability_runtime_inner_api_path}/session_handler/include",

    "${window_base_path}/wm/include",
    "${window_base_path}/wm/include/zidl",

    # for window_manager_hilog
    "${window_base_path}/utils/include",

    "${accessibility_path}/interfaces/innerkits/common/include/",
    "${accessibility_path}/common/interface/include/parcel/",
  ]
}

ohos_static_library("ws_unittest_common") {
  visibility = [ ":*" ]
  testonly = true

  public_configs = [
    ":ws_unittest_common_public_config",
    "${window_base_path}/resources/config/build:coverage_flags",
    "${window_base_path}/resources/config/build:testcase_flags",
  ]

  deps = [
    "${window_base_path}/dm:libdm",
    "${window_base_path}/utils:libwmutil",
    "${window_base_path}/utils:libwmutil_base",
    "${window_base_path}/window_scene/common:window_scene_common",
    "${window_base_path}/window_scene/intention_event/service:intention_event_anr_manager",
    "${window_base_path}/window_scene/screen_session_manager:screen_session_manager_client",
    "${window_base_path}/window_scene/session:scene_session",
    "${window_base_path}/window_scene/session:screen_session",
    "${window_base_path}/window_scene/session_manager:scene_session_manager",
    "${window_base_path}/window_scene/session_manager:screen_session_manager",
    "${window_base_path}/window_scene/session_manager:session_manager",
    "${window_base_path}/wm:libwm",
    "//third_party/googletest:gmock",
    "//third_party/googletest:gtest_main",
    "//third_party/libxml2:libxml2",
  ]

  public_deps = [
    "${arkui_path}/napi:ace_napi",
    "${graphic_base_path}/graphic_2d/rosen/modules/render_service_client:librender_service_client",
    "${window_base_path}/utils:libwmutil",
    "${window_base_path}/utils:libwmutil_base",
  ]

  external_deps = [
    "ability_base:configuration",
    "ability_base:want",
    "accessibility:accessibility_common",
    "accessibility:accessibility_interface",
    "c_utils:utils",
    "hilog:libhilog",
    "input:libmmi-client",
    "ipc:ipc_single",
  ]
  defines = []
  if (defined(global_parts_info) && defined(global_parts_info.sensors_sensor)) {
    external_deps += [ "sensor:sensor_interface_native" ]
    defines += [ "SENSOR_ENABLE" ]
  }

  part_name = "window_manager"
  subsystem_name = "window"
}
## Build ws_unittest_common.a }}}
